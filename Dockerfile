FROM ubuntu:16.04
MAINTAINER Roman_Kostohryz
#Install wget open-jdk-8 and jenkins
ARG user=jenkins
ARG group=jenkins
ARG uid=1000
ARG gid=1000
ARG JENKINS_HOME=/var/jenkins_home
RUN mkdir -p $JENKINS_HOME \
  && chown ${uid}:${gid} $JENKINS_HOME \
  && groupadd -g ${gid} ${group} \
  && useradd -d $JENKINS_HOME -u ${uid} -g ${gid} -m -s /bin/bash ${user}

RUN apt update -y && apt upgrade -y &&\
    apt install curl wget -y &&\
    wget -q -O - https://pkg.jenkins.io/debian/jenkins-ci.org.key | apt-key add - &&\
    echo deb http://pkg.jenkins.io/debian-stable binary/ | tee /etc/apt/sources.list.d/jenkins.list
RUN apt update -y &&\
    apt install jenkins -y
WORKDIR $JENKINS_HOME
# Install jenkins plugin
RUN apt update -y &&\
    apt install openjdk-8-jdk openjdk-8-jre -y &&\
    wget https://updates.jenkins-ci.org/download/war/2.121.2/jenkins.war
    #mount  volumes
EXPOSE 8080
ENTRYPOINT [ "java", "-jar", "jenkins.war" ]